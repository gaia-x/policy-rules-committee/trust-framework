# Service & Subclasses

Here is the main model for service composition, also included in the Gaia-X Architecture document.

A `Service Offering` can be associated with other `Service Offering`s.

```mermaid
classDiagram
    ServiceOffering o-- Resource: aggregationOf
    Resource o-- Resource: aggregationOf

class Resource{
    <<abstract>>
}

    Resource <|-- "subClassOf" PhysicalResource: 
    Resource <|-- "subClassOf" VirtualResource
    VirtualResource "1" <-- "*" InstantiatedVirtualResource : instanceOf

    ServiceOffering "1" ..> "1" Participant : providedBy

    PhysicalResource "1" ..> "0..*" Participant : ownedBy
    PhysicalResource "1" ..> "0..*" Participant : manufacturedBy
    PhysicalResource "1" ..> "1..*" Participant : maintainedBy

    VirtualResource "1" ..> "1..*" Participant : copyrightOwnedBy

    InstantiatedVirtualResource --> "1" Resource : hostedOn

    InstantiatedVirtualResource <..> ServiceInstance : equivalent to
    InstantiatedVirtualResource "1" ..> "1..*" Participant : tenantOwnedBy
    InstantiatedVirtualResource "1" ..> "1..*" Participant : maintainedBy
``` 

## Service offering

This is the generic format for all service offerings

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `name`                 | 0..1     | State        | A human readable name of the component |
| `providedBy`           | 1     | State        | a resolvable link to the `participant` self-description providing the service    |
| `aggregationOf[]`      | 0..*  | State        | a resolvable link to the `resources` self-description related to the service and that can exist independently of it. |
| `dependsOn[]`          | 0..*  | State        | a resolvable link to the `service offering` self-description related to the service and that can exist independently of it. |
| `termsAndConditions[]` | 1..*  | State        | a resolvable link to the Terms and Conditions applying to that service. |
| `policy[]`             | 1..*  | State        | a list of `policy` expressed using a DSL (e.g., Rego or ODRL) (access control, throttling, usage, retention, ...) |
| `dataProtectionRegime[]`| 0..* | State        | a list of data protection regime from the list available below |
| `dataAccountExport[]`   | 1..*  | State       | list of methods to export data from your user's account out of the service |

**termsAndConditions structure**

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `URL`                  | 1     | State        | a resolvable link to document    |
| `hash`                 | 1     | State        | sha256 hash of the above document. |

**dataAccountExport structure**

The purpose is to enable the participant ordering the service to assess the feasability to export its personal and non-personal data out of the service.  
This export shall cover account data e.g., account holder's billing information, information on the PII held - but also data provided previously to the service by the user.

| Attribute     | Card. | Trust Anchor | Comment                                         |
|---------------|-------|--------------|-------------------------------------------------|
| `requestType` | 1     | State        | the mean to request data retrieval: `API`, `email`, `webform`, <br> `unregisteredLetter`, `registeredLetter`, `supportCenter` |
| `accessType`  | 1     | State        | type of data support: `digital`, `physical` |
| `formatType`  | 1     | State        | type of Media Types (formerly known as MIME types) as defined by the [IANA](https://www.iana.org/assignments/media-types/media-types.xhtml). |

<details>
  <summary>Examples of <code>dataAccountExport</code></summary>

```json
{
  "dataAccountExport": [
    {
      "requestType": "API",
      "accessType": "digital",
      "formatType": "application/json"
    },
    {
      "requestType": "supportCenter",
      "accessType": "physical",
      "formatType": "application/octet-stream"
    }
  ]
}
```

</details>


** Personal Data Protection Regimes**
Gaia-X strives to contribute compliance with the protection of personal data, i.e., privacy. 
Gaia-X envisages its implementation internationally, whilst not neglecting its European sources. 

Conformity with specific requirements deriving from different regimes protecting personal data, will be subject to respective Labels. 
Nonetheless, it is considered significant added value, to allow Customers to filter Service Offerings by applicable regimes. 

Thus, Service Offerings shall be able to identify Personal Data Protection Regimes in their respective Self Description. Such identified regimes do not entail any trusted or verified statement of compliance. 

Non-exclusive list of Personal Data Protection Regimes:

- `GDPR2016`: [General Data Protection Regulation](https://eur-lex.europa.eu/eli/reg/2016/679/oj) / EEA
- `LGPD2019`: [General Personal Data Protection Law](http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2018/lei/l13709.htm) (_Lei Geral de Proteção de Dados Pessoais_) / BRA
- `PDPA2012`: [Personal Data Protection Act 2012](https://sso.agc.gov.sg/Act/PDPA2012) / SGP
- `CCPA2018`: [California Consumer Privacy Act](https://oag.ca.gov/privacy/ccpa) / US-CA
- `VCDPA2021`: [Virginia Consumer Data Protection Act](https://lis.virginia.gov/cgi-bin/legp604.exe?212+ful+CHAP0036+pdf) / US-VA

The participant may wish to refer to ISO/IEC 27701 as a tool for comparing the various data protection regimes.

To enable interoperability, it is strongly recommended to refer to the options as provided in the [Gaia-X Registry](https://registry.gaia-x.eu). The Gaia-X Registry will allow for the possibility to select one or several of such regimes, as well as the possibility to add additional regimes. Maintenance of the registry shall regularly assess the individually identified regimes and - where appropriate - add such regimes to the pre-defined list of Personal Data Protection Regimes. 


**Consistency rules**

- the keys used to sign a SERVICE OFFERING description and the `providedBy` PARTICIPANT description should be from the same keychain.

**Compliance with Criteria of Policy Rules and Labelling Document (PRLD)**
In cases, where there is no additional attribute defined in the Trust Framework or the PRLD, and where the PRLD refers to the Trust Framework as verification process, it shall - for the time being - suffice that the Cloud Service Provider submits a self-signed Verified Credential indicating the relevant Criterion alongside the statement of implementation by "true" or "false".

The syntax shall be as follows:


| Attribute     | Card. | Trust Anchor | Comment                                         |
|---------------|-------|--------------|-------------------------------------------------|
| `critXX`       | 0..1     | `providedBy` | Boolean: "True"/"False"    |


Those criteria are signed by the Participant providing the service

As an option, the Cloud Service Provider shall have the possibility to add a string identifying any third-party standards that might be of relevance.


## Service Instance / Instantiated Virtual Resource

An Instantiated Virtual Resource is an instance from a Virtual Resource.  
An Instantiated Virtual resource is a running resource exposing endpoints such as, and not limited to, a running process, an online API, a network connection, a virtual machine, a container, an operating system. 

| Attribute            | Card. | Trust Anchor | Comment                                   |
|----------------------|-------|--------------|-------------------------------------------|
| `maintainedBy[]`     | 1..*  | State        | a list of `participant` maintaining the resource in operational condition. |
| `hostedOn`           | 1     | State        | a `resource` where the process is located (physical server, datacenter, availability zone, ...). |
| `instanceOf`         | 1     | State        | a `virtual resource` (normally a `software` resource) this process is an instance of. |
| `tenantOwnedBy[]`    | 1..*  | State        | a list of `participant` with contractual relation with the resource. |
| `serviceAccessPoint[]`| 1..*  | State        | a list of [Service Access Point](https://en.wikipedia.org/wiki/Service_Access_Point) which can be an endpoint as a mean to access and interact with the resource |

**Service Access Point**

A service access point is an identifying label for network endpoints used in the [OSI model](https://en.wikipedia.org/wiki/OSI_model). 

The format below doesn't represent all possible service access point types.

| Attribute   | Card. | Trust Anchor | Comment                    |
|-------------|-------|--------------|----------------------------|
| `name`      | 0..1     | State        | name of the endpoint       |
| `host`      | 0..1     | State        | host of the endpoint       |
| `protocol`  | 0..*     | State        | protocol of the endpoint   |
| `version`   | 0..1     | State        | version of the endpoint    |
| `port[]`    | 0..*  | State        | port of the endpoint       |
| `openAPI`   | 0..*     | State        | URL of the [OpenAPI documentation](https://github.com/OAI/OpenAPI-Specification/blob/3.1.0/versions/3.1.0.md) |


<details>
  <summary>Difference between <code>Virtual Resource</code> and <code>Instantiated Virtual Resource</code></summary>

<img src="../figures/vr-vs-ivr.png" alt="Virtual Resource vs Instantiated Virtual Resource">

In <code>red</code>, a <code>VirtualResource</code>.<br>
In <code>blue</code>, an <code>InstantiatedVirtualResource</code>.<br>
The <code>InstantiatedVirtualResource</code> exposes <code>serviceAccessPoint</code> configured with Access Control, used by Participant to access other Resource.

</details>

## Data exchange component

A `data exchange component` offering, including but not limited to, a connector, a platform, a service bus, a messaging or an event streaming platform or REST APIs, must fulfil those requirements:

<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

The data exchange component is a service to negotiate (signaling layer) and exchange dataset (media layer).

The negotiation shall be performed based on the policies from the `policy` attribute of the Gaia-X Self-Description of the entities - Participants, Services, Resources - involved in the data transaction.

Note: in case at least one of the Gaia-X Self-Description of the entities involved in the data transaction contains policies that can't be understandable by an algorithm, the data exchange component can fallback to manual negotiation.

**Consistency rules**

- the service offering shall contain or point to a InstantiatedVirtualResource self-description with at least one serviceAccessPoint with a name "contractNegotiationEndpoint".
- the output of the "contractNegotiationEndpoint" shall point to the result of the negotiation signed by all the Participants in direct link with the negotiation. (Note: direct link is defined by all Participants returned by a Breadth-first search algorithm, starting from the Data Resource being negotiated, with a path length of 1)

<details>
<summary>Example of valid JSONPath for validating <code>contractNegotiationEndpoint</code></summary>

The following JSONPath must return at least one object from a service offering self-description.

```shell
$.aggregationOf[?(@.type == "InstantiatedVirtualResource")].serviceAccessPoint[?(@.name == "contractNegotiationEndpoint")]
```

</details>

## Verifiable Credential Wallet

A `wallet` enables to store, manage, present Verifiable Credentials:
<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

| Attribute                            | Card. | Trust Anchor | Comment                                         |
|--------------------------------------|-------|--------------|-------------------------------------------------|
| `verifiableCredentialExportFormat[]` | 1..*  | State        | a list of machine readable formats used to export verifiable credentials. |
| `privateKeyExportFormat[]`           | 1..*  | State        | a list of machine readable formats used to export private keys. |

## Interconnection

An `interconnection` enables a mutual connection between two or more elements.

| Attribute                               | Card. | Trust Anchor | Comment                                         |
|-----------------------------------------|-------|--------------|-------------------------------------------------|
| `location[].countryCode`                | 2..*  | State        | a list of physical locations in ISO 3166-2 alpha2, alpha-3 or numeric format with at least the both ends of the connection. |

## Catalogue

A `catalogue` service is a subclass of `serviceOffering` used to browse, search, filter services and resources.

| Attribute                    | Card. | Trust Anchor | Comment                                         |
|------------------------------|-------|--------------|-------------------------------------------------|
| `getVerifiableCredentialsIDs`| 1..*  | State        | a route used to synchronize catalogues and retrieve the list of Verifiable Credentials (`issuer`, `id`). [1] |

[1]: Using the Verifiable Credential terms, it's up to the `issuer` to control and decide if the credential's [`id`](https://www.w3.org/TR/vc-data-model/#identifiers) can be dereferenced and the `holder` to implement access control of the verifiable credentials.

## Interoperability, Portability, Switchability and Intellectual Property Protection (experimental)

This section introduces important Gaia-X principles.  
While the format of the attributes are not yet defined, the sections below give insights about the future requirements.

### Interoperability

This covers how the service offering can **communicate** with another service, such as making or responding to requests.

The aim of the future attributes is to provide:

- a list of agreements in place with other service providers to ensure interoperability between systems
- a list of URL to interoperability capabilities and transparency documentation

See [ISO/IEC 19941:2017](https://standards.iso.org/ittf/PubliclyAvailableStandards/c066639_ISO_IEC_19941_2017.zip)-5.2.1 for a description of the facets that need to be addressed.

### Data Portability

This refers to the porting of **data** (structured or unstructured) from one cloud service to another, public or private.

Note: Data portability will inevitably be limited to Customer Data and a _subset_ of Derived Data as determined by contract, privacy regulations, etc. See the `ServiceOffering.dataAccountExport` attribute.

The aim of the future attributes is to provide:

- a list of publicly filed declarations of adherence such as [SWIPO Code of Conduct](https://swipo.eu/current-swipo-code-adherences/)

If a Gaia-X service offering includes multiple cloud services with different data portability capabilities, it is expected to have separate Self-Descriptions for each type of data portability capabilities.

Note 2: The SWIPO codes of conduct do not cover all cloud service categories (e.g. there is no specific SWIPO Code for PaaS), so it is not possible to mandate this attribute for all services.

See [ISO/IEC 19941:2017](https://standards.iso.org/ittf/PubliclyAvailableStandards/c066639_ISO_IEC_19941_2017.zip)-5.2.2 for a description of the facets that need to be addressed.

### Application Portability

This refers to the porting of customer or third party **executable code** from one cloud service to another, public or private.

Note: Application portability is highly complex, especially when it comes to identity management, license, access control lists, privacy controls, transfer of credentials, etc.

The aim of the future attributes is to provide:

- a list of URL pointing to a declaration of adherence such as [SWIPO Code of Conduct](https://swipo.eu/current-swipo-code-adherences/)
- a list of URL pointing to a declaration of whether and how the application license can be switched to another service provider

If a Gaia-X service offering includes multiple cloud services with different data portability capabilities, it is expected to have separate Self-Descriptions for each type of data portability capabilities.

Note 2: For [SWIPO Code of Conduct](https://swipo.eu/current-swipo-code-adherences/):

- This can apply to executable code within an IaaS virtual machine instance, or code that can be executed within a larger SaaS application (such as scripts or SQL stored procedures).
- This codes of conduct do not cover all cloud service categories (e.g. there is no specific SWIPO Code for PaaS), so it is not possible to mandate this attribute for all services.

See [ISO/IEC 19941:2017](https://standards.iso.org/ittf/PubliclyAvailableStandards/c066639_ISO_IEC_19941_2017.zip)-5.2.3 for a description of the facets that need to be addressed.

### Switchability

This refers to moving a customer's **account** from one service provider to another, including the change of contracts and necessary technical changes.

Service switching is highly complex. Moving a customer to another cloud application provider requires identifying a destination which is in the same broad category of cloud service offering, and where the common functionality offered by the original service and the destination meets the minimum business needs of the customer wishing to switch. Since all services evolve constantly this is a moving target, and each customer will have different criteria. In addition, there can be considerable legal issues to be addressed. This includes the "policy" facets of data and application portability as described in ISO/IEC 19941:2017, but also the unique specifics of contracts and any possible implications for security and this continued compliance with privacy and data protection regulations. Responsibility for each stage of the switching process (including security, liability, and privacy compliance) will need to be determined and agreed in advance by all parties.

The aim of the future attributes is to provide:

- a list of URL pointing to a declaration of whether and how a cloud service contract can be switched to another service provider
- a list of URL pointing to required or available policies for switching
- a list of URL pointing to a declaration of whether and how the Identity and Access Management function can be switched to another service provider

Note 1: At present there do not seem to be any established Codes of Conduct or other frameworks that cover these aspects.

<!-- Note 2: Not all cloud services provide Identity and Access management, so it is not possible to mandate this attribute. -->

<!--
| 1.x     | `virtualLocation[]`  | 1..*  | State        | a list of location such an Availability Zone, network segment, IP range, AS number, ... (format to be specified in a separated table) |
-->

### Intellectual Property Protection
The term Intellectual property (IP) refers to unique and value-adding creations of the human mind. IP enables the granting of property-like rights over new discoveries, new knowledge and creative expressions. IP relates to but is not limited to know-how, products, processes, software, data, and artistic works. The rights resulting from various forms of IP are referred to as intellectual property rights (IPR). IPRs provide ownership, i.e. the capacity to grant the IP usage rights (licensing), to assign the IP and rights to exclude third parties from using what is protected to the owner. Trust is paramount in the willingness for partners to collaborate and share information. IPRs can ensure the confidence of the partners that their knowledge capital and investment will be respected in the frame of acknowledged context and therefore that the use of the information shared will remain in the agreed context for the agreed duration. IPRs can exist in the form of a Patent, Utility model, Copyright, Trademark, Industrial design, Geographical Indication or Trade Secret (see [ WIPO](https://www.wipo.int/edocs/pubdocs/en/wipo_pub_895_2016.pdf) and [ISO 56005:2020(en)](https://www.iso.org/obp/ui/#iso:std:iso:56005:ed-1:v1:en))
 
In the context of the Gaia-X Trust Framework, IP can be declared on `Service Offering`, `PhysicalResource`, `SoftwareResource` and / or `DataResource` level.


| Attribute                    | Card. | Trust Anchor | Comment                                         |
|------------------------------|-------|--------------|-------------------------------------------------|
| `intellectualPropertyOwner[]`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]`        | An IP owner is a person or organisation that has the ownership and has the right to decide how the creation can be used by others. The attribute expects a list of owners either as a free form string or or URIs from which Self-Descriptions can be retrieved. |
| `intellectualPropertyOwnershipType`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]` | Declaration of the type of ownership type ([Patent](https://www.wipo.int/patents/en/), [Utility model](https://www.wipo.int/patents/en/topics/utility_models.html), [Copyright](https://www.wipo.int/copyright/en/), [Trademark](https://www.wipo.int/trademarks/en/), [Industrial design](https://www.wipo.int/designs/en/), [Geographical Indication](https://www.wipo.int/geo_indications/en/) and [Trade Secret](https://www.wipo.int/tradesecrets/en/)) according to WIPO. |
| `iPRegistrationProof[]`| 0..*  | State        | Legally binding proof that verifies the registration of the declared ownership type. The proof can be preferably provided by an URL. <br> Sources might include: <br>- Patent: [global Patentscope for WIPO](https://patentscope.wipo.int/search/en/search.jsf) and [Espacenet for EU](https://www.epo.org/searching-for-patents/technical/espacenet.html) <br/>- Trademark: [Brand name database](https://branddb.wipo.int/en/quicksearch?by=brandName&v=&rows=30&sort=score%20desc&start=0&_=1681826804155) (WIPO); [eSearch Plus at EU level](https://www.epo.org/searching-for-patents/technical/espacenet.html) and [TM view](https://www.tmdn.org/tmview/#/tmview) for all EU countries at national level <br/>- Industrial design: [Global design database](https://www3.wipo.int/designdb/en/index.jsp) and at EU level [Design view](https://www.tmdn.org/tmdsview-web/welcome#/dsview) <br/>Alternatively, a certificate has to be provided. <br/> In case of a Trade Secret, a NDA/Confidentiality Agreement or IPR Agreement can be provided. <br/> If none of the above solutions can be made available, a self-claim must be provided.
| `iPExpirationDate`| 0..1  | State        | Date expected in the ISO 8601 format. The terms of coverage can be cross-checked with the WIPO [Terms of Protection](https://www.wipo.int/export/sites/www/standards/en/pdf/archives/03-09-02arc2008.pdf) |
| `geographicValidity[]`| 0..*  | State        | List of the Countries which are covered by `iPRegistrationProof[]` in ISO 3166-2 alpha2, alpha-3 or numeric format. Patent under EU Unitary Patent System (from 1 June 2023) valid in contracting States. Patents under the [EU Unitary Patent System](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2012:361:0001:0008:en:PDF) will be from 1 June 2023 valid in [contracting States](https://www.epo.org/applying/european/unitary/unitary-patent.html).   |
| `usageRightsAgreements[]`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]`        | Description of the usage rights allowing a person or organisation the right to use the intellectual property. Such an agreement may be limited in time and context of the usage.  <br> The [WIPO Green licensing checklist](https://www3.wipo.int/wipogreen/docs/en/wipogreen_licensingchecklist_061216.pdf) and [EU factsheet](https://op.europa.eu/en/publication-detail/-/publication/e510929d-f015-11eb-a71c-01aa75ed71a1/language-en/format-PDF/source-227478787) on licence agreement helps to identify the issues that might be encountered in negotiating an agreement (contract) that relates to intellectual property and technology.|
| `usageRightsExpirationTime`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]`        | A date time in ISO 8601 format after which `usageRightsAgreement[]` is expired.|
| `usageRightsExpirationCondition[]`| 0..*  | `providedBy`, `ownedBy[]` or `producedBy[]`        | A list of conditions after which usageRightsAgreement[] is expired. The Conditions can be expressed using ODRL. |

 
**Consistency Rules**
- For every declaration, a unique ID (`intellectualPropertyID`) will be automatically generated and assigned.All other declared attributes in the section have to reference to that ID.
- The declaration of IP can be on service level (identifiable thru the name and `providedBy`) and/or resource level (identifiable thru `ownedBy[]` and `producedBy[]`). The `intellectualPropertyID` has to link to one or multiple unique `Service Offering`, `PhysicalResource`, `SoftwareResource` and / or `DataResource`. 
- If IP is wished to be declared, `intellectualPropertyOwner[]`, `intellectualPropertyOwnershipType[]`, `iPRegistrationProof[]`,`iPExpirationDate`, `geographicValidity` and `usageRightsAgreements[]` are mandatory.
- `usageRightsExpirationCondition` and `usageRightsExpirationTime` have to be linked to one or multiple usageRightsAgreements within `usageRightsAgreements[]`.


