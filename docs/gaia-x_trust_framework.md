# Gaia-X Trust Framework

The Gaia-X Trust Framework is the set of rules that define the minimum baseline to be part of the **Gaia-X Ecosystem**. Those rules provide a common governance and the basic level of interoperability across individual ecosystems while letting the users in full control of their choices.

The Gaia-X Trust Framework operationalizes the requirements as defined by Gaia-X, e.g, in the Policy Rules and Labelling Document, or the Architecture Document - where the latter especially allows for federated and interoperable Gaia-X ecosystems. 

The Trust Framework foresees verifiable credentials and linked data representations as cornerstone of its future operations. Trusted information shall be retrieved in machine readable manners, and where such manners are missing, Gaia-X will define processes to translate trusted information in a machine readable format. This is a prerequisite of federating trusted statements within the Gaia-X Ecosystem and developing mechanisms to re-assess validity of claims within the Trust Framework.

The set of computable rules known as compliance process is automated and versioned. It means that this document will also be versioned. This version of the Gaia-X Trust Framework document replaces previous versions of this document.

## Trust Framework scope

Those rules apply to **all** Gaia-X Self-Descriptions and there is a Self-Description for **all** the entities defined as part of the Gaia-X Conceptual model described in the Gaia-X Architecture document:

This list mainly comprises:

- Participant including Consumer, Federator, Provider
- Service Offering
- Resource

In the text below, the term 'federation' will be used as a generic term for ecosystems and data spaces.

The Trust Framework can be extended by a federation as follow:

- a federation can add more requirements on the eligibility of the Gaia-X Trust Anchors, hence selecting a subset of the Gaia-X Trust Anchors for the federation's domain.
- a federation can add additional rules on top of the ones in this document by:
    - adding more criteria for a Self-Description type. Example: adding requirements for a participant to join the federation, enforcing a deeper level of transparency for a Service Offering
    - selecting new domain specific Trust Anchors for the new criteria.

**! warning !**: For an already defined criteria, it will break Gaia-X Compliance to extend the list of Gaia-X Trust Anchors eligible to sign the criteria.

![Ecosystem rules](figures/ecosystem_rules.svg)

$$\mathbb{R}_{GaiaX} \subseteq \mathbb{R}_{domain}$$

| Rule property                                 | A domain refining Gaia-X Compliance <br>$\forall r \in \mathbb{R}_{GaiaX} \text{ and } r_{domain} \in \mathbb{R}_{domain}$ | A domain extending Gaia-X Compliance<br>$\forall r \in \mathbb{R}_{domain} \setminus \mathbb{R}_{GaiaX}$ |
|-----------------------------------------------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| Attribute name: $r_{name}$                           | $r_{name_{CamelCase}} \equiv r_{name_{snake\_case}}$                               | $r_{name} \notin \mathbb{R}_{GaiaX}$                             |
| Cardinality: $\lvert r \lvert$               | $\lvert r_{domain} \lvert \ge \lvert r \lvert$                                     | _no restriction_                                                   |
| Value formats: $r \rightarrow \texttt{VF}(r)$          | $\texttt{VF}(r_{domain}) \subseteq \texttt{VF}(r)$                                       | _no restriction_                                                   |
| Trust Anchors: $r \rightarrow \texttt{TA}(r)$         | $\texttt{TA}(r_{domain}) \subseteq \texttt{TA}(r)$                             | _no restriction_                                                   |
| Trusted Data Sources: $r \rightarrow \texttt{TDS}(r)$ | $\texttt{TDS}(r_{domain}) \subseteq \texttt{TDS}(r)$                 | _no restriction_                                                   |

### Gaia-X Labelling Criteria

The Labelling Criteria are further detailed and translated into concrete criteria and measures in the [Gaia-X Policy Rules and Labelling Document](https://www.gaia-x.eu/publications). This document links back to the [Gaia-X Labelling Framework paper](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf) which was published in November 2021.

| Framework           | Notes |
|---------------------|-------|
| Trust Framework     | Compulsory set of rules to be part of the Gaia-X Ecosystem. <br/> Federations can extend such rules. |
| Gaia-X Policy Rules and Labelling Document | The Policy Rules define high-level objectives safeguarding the added value and principles of the Gaia-X ecosystem.The intent of the policy rules is to identify clear controls to demonstrate European values of Gaia-X, such values including openness, transparency, data protection, security, and portability. To allow for validation, the high-level objectives are underpinned by the Gaia-X Labelling Criteria. The Criteria are an optional set of criteria for Service Offerings. | 

### Gaia-X Compliance software deliverables
 
To operationalise the Gaia-X Compliance, its software implementations must be executed and turned into up and running services, providing traceable evidence of correct executions.

While the governance of the Gaia-X Compliance rules and process is and will stay under the control of the Gaia-X Association, the Gaia-X Compliance services will go through several mutually non-exclusive deployments scenario[^article-compliance-deployment].

[^article-compliance-deployment]: <https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/>

During the pilot phase, the current, non-final deployment is described below.

| Deliverables           | Notes |
|---------------------|-------|
| [Gaia-X Compliance Service](https://compliance.gaia-x.eu)     | This service validates the shape, content and credentials of Self Descriptions and issues a Verifiable Credential attesting of the result. Required fields and consistency rules are defined in this document. Format of the input and output are defined in the [Identity, Credential and Access management document](https://docs.gaia-x.eu/).|
| [Gaia-X Registry](https://registry.gaia-x.eu) | This service provides a list of valid shapes, and valid and revoked public keys. The Gaia-X Registry will also be used as the seeding list for the network of catalogues. More information in the Architecture document: [Gaia-X Registry](https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#gaia-x-decentralized-autonomous-ecosystem). |

## Gaia-X Self-Description

Gaia-X Self-Descriptions are:

- machine readable texts
- cryptographically signed, preventing tampering with its content
- following the Linked Data principles[^linkeddata] to describe attributes
[^linkeddata]: <https://www.w3.org/standards/semanticweb/data>

The format is following the [W3C Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/).  Its usage in Gaia-X is specified in the [chapter "Credential Format" of the document "Gaia-X Federation Services - GXFSv2 Identity and Access Management"](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/22.10/credential_format/).

## Gaia-X Trust Framework

There are 4 types of rules:

- serialization format and syntax.
- cryptographic signature validation and validation of the keypair associated identity.
- attribute value consistency.
- attribute veracity verification.
